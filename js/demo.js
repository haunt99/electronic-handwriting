let dyCanvas = new DyCanvas({
  el: "canvas",
  color: "#000",
  bgColor: "#d3d3d3",
  lineWidth: 8,
  brush: true,
});
dyCanvas.initCvs();
